This is a free Software and published under GPLv3 License (Copyright Tom C. Adler, 2017).

# What is NOQON

This is a learning app for Android based on the Interactive Tutoring Feedback Model. This model comes from the field of learning psychologie for self regulated computer based learning and is already well established. 
You can find more information on this pages: 
* [research site of the Interactive Tutoring Feedback Model (not much and maybe a little outdated, but a good start)](https://tu-dresden.de/mn/psychologie/lehrlern/forschung/projekte/feedback?set_language=en "TU Dresden")
* [paper, which is a good introduction to the model](https://www.phil.uni-passau.de/fileadmin/dokumente/lehrstuehle/narciss/Narciss__S.__2013_._Designing_and_Evaluating_Tutoring_Feedback_Strategies.pdf "Design and Evaluation Tutoring Feedback Strategies")

**Screenshots:** You can find some impression in the directory */screenshots*

# Current Features

* Multiple Choice Question
* two level feedback mechanism
* text and pictures in introduction and question parts
* high scalable database management for all content

# Next Features
* user-profile with persistent saving of the learning procress
* individual, competence based feedback based on you learning procress
* more question types ("Fill in Blanks", "Drag and Drop Tasks")
* easy to use interface for adding, editing and managing learning content

# Reuse and/or Contribution

The software is one product of a master thesis for a learning app about quantum physics. So it's well documentated. The architecture of the software is based on common principles of professional software and database development.
It will be easy to reuse the code, add more features, improve the design and usabiltiy and so on. If you are interested in using this code, feel free to contact me under tocto[at]mailbox.org.
I can supply you and this page with a lot more documentation in short time and I am willing to invest this efforts, if you consider this software for your use.


