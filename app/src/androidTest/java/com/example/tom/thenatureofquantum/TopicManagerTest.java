package com.example.tom.thenatureofquantum;

import android.app.Instrumentation;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by tom on 28.07.17.
 */
@RunWith(AndroidJUnit4.class)

public class TopicManagerTest {


    List<Topic> expectedTopicList;
    List<Sequence> expectedSequenceList;
    List<Unit> expectedUnitList;

    @Before
    public void setUp() throws Exception {

        expectedUnitList = new ArrayList<>();
        expectedSequenceList = new ArrayList<>();
        expectedTopicList = new ArrayList<>();

        Set<String> answerOptions1 = new HashSet<>();
        Set<String> answerOptions2 = new HashSet<>();
        Set<String> solution1 = new HashSet<>();
        Set<String> solution2 = new HashSet<>();

        answerOptions1.add("Interferenz");
        answerOptions1.add("Trägheitsgesetz");
        answerOptions1.add("Lichtstrahlen");
        answerOptions1.add("Massepunkt");

        solution1.add("Lichtstrahlen");
        solution1.add("Massepunkt");

        answerOptions2.add("Verstärkung");
        answerOptions2.add("Auslöschung");
        answerOptions2.add("Farben des Lichtes");
        answerOptions2.add("Vergrößerung an Linsen");

        solution2.add("Verstärkung");
        solution2.add("Auslöschung");
        solution2.add("Farben des Lichtes");

        expectedUnitList.add(
                new Unit("Was ist ein physikalisches Modell?", new Content("TODO\n" +
                        "Ein physikalisches Modell. …", null),
                        new Question("Markiere die Begriffe, welche Modelle der Physik repräsentieren?",
                                answerOptions1,
                                solution1,
                                "Die Interferenz ist ein Phänomen und das Trägheitsgesetz ist ein Gesetz, welches ein Phänomen vorhersagt.")));

        expectedUnitList.add(
                new Unit("Leistungsfähigkeit und Ähnlichkeit von Modellen",
                        new Content("Es existieren unterschiedliche Modelle, für gleiche Phänomene. Jedes Modell muss für sich allein in der Lage sein, das Phänomen zu beschreiben und zu erklären!\n" +
                                "Modelle können aber auch aufeinander aufbauen.\n" +
                                "\n" +
                                "TODO\n" +
                                "\n", null),
                        new Question("Welche Phänomene des Lichtes kann das Wellenmodell erklären, aber das Strahlenmodell nicht?",
                                answerOptions2,
                                solution2,
                                null)));

        expectedSequenceList.add(new Sequence("Der Modellbegriff", expectedUnitList));
        expectedSequenceList.add(new Sequence("Was sind Quanten und was sind sie nicht?", null));
        expectedSequenceList.add(new Sequence("Die Rolle des Messprozesses in der Quantenphysik", null));

        expectedTopicList.add(new Topic("Grundlagen", "Welche Vorstellungen darf man von einer Welt haben, die man nicht sehen kann, nicht hören kann, nicht erleben kann?Was sind Objekte sind Quantenobjekte? Was lässt sie eine solche Faszination ausstrahelen? Fragen über Fragen von denen zunächst die grundlegendsten geklärt werden sollen.", expectedSequenceList));
    }

    @Test
    public void getTopics() throws Exception {
//        todo first implement equals method
//        TopicManager.initializeTopics(InstrumentationRegistry.getTargetContext());
//
//        assertEquals("should be the same Topic List", TopicManager.getTopicList(), expectedTopicList);
//
//        Topic topic = TopicManager.getTopicList().get(0);
//        TopicManager.setSelectedTopic(topic);
//
//        assertEquals("should be same sequence list", TopicManager.getSelectedSequence(), expectedSequenceList);
//
//        Sequence seq = TopicManager.getSelectedTopic().getSequencelist().get(0);
//        TopicManager.getSelectedTopic().setSelectedSequence(seq);
//
//        assertEquals("should be same unit list", expectedUnitList, TopicManager.getSelectedSequence().getUnitList());
//
//        Unit unit = TopicManager.getSelectedSequence().getUnitList().get(0);


    }

}