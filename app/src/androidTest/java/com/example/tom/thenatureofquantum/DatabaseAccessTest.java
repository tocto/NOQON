package com.example.tom.thenatureofquantum;

import android.app.Instrumentation;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by tom on 28.07.17.
 */
@RunWith(AndroidJUnit4.class)

public class DatabaseAccessTest {

    private Context context;

    DatabaseAccess db;

    List<Topic> expectedTopicList;
    List<Sequence> expectedSequenceList;
    List<Unit> expectedUnitList;

    @Before
    public void setUp() throws Exception {

        expectedUnitList = new ArrayList<>();
        expectedSequenceList = new ArrayList<>();
        expectedTopicList = new ArrayList<>();

        db = DatabaseAccess.getInstance(InstrumentationRegistry.getTargetContext());

        Set<String> answerOptions1 = new HashSet<>();
        Set<String> answerOptions2 = new HashSet<>();
        Set<String> solution1 = new HashSet<>();
        Set<String> solution2 = new HashSet<>();

        answerOptions1.add("Interferenz");
        answerOptions1.add("Trägheitsgesetz");
        answerOptions1.add("Lichtstrahlen");
        answerOptions1.add("Massepunkt");

        solution1.add("Lichtstrahlen");
        solution1.add("Massepunkt");

        answerOptions2.add("Verstärkung");
        answerOptions2.add("Auslöschung");
        answerOptions2.add("Farben des Lichtes");
        answerOptions2.add("Vergrößerung an Linsen");

        solution2.add("Verstärkung");
        solution2.add("Auslöschung");
        solution2.add("Farben des Lichtes");

        expectedUnitList.add(
                new Unit("Was ist ein physikalisches Modell?", new Content("TODO\n" +
                "Ein physikalisches Modell. …", null),
                new Question("Markiere die Begriffe, welche Modelle der Physik repräsentieren?",
                        answerOptions1,
                        solution1,
                        "Die Interferenz ist ein Phänomen und das Trägheitsgesetz ist ein Gesetz, welches ein Phänomen vorhersagt.")));

        expectedUnitList.add(
                new Unit("Leistungsfähigkeit und Ähnlichkeit von Modellen",
                new Content("Es existieren unterschiedliche Modelle, für gleiche Phänomene. Jedes Modell muss für sich allein in der Lage sein, das Phänomen zu beschreiben und zu erklären!\n" +
                "Modelle können aber auch aufeinander aufbauen.\n" +
                "\n" +
                "TODO\n" +
                "\n", null),
                new Question("Welche Phänomene des Lichtes kann das Wellenmodell erklären, aber das Strahlenmodell nicht?",
                        answerOptions2,
                        solution2,
                        null)));

        expectedSequenceList.add(new Sequence("Der Modellbegriff", expectedUnitList));
        expectedSequenceList.add(new Sequence("Was sind Quanten und was sind sie nicht?", null));
        expectedSequenceList.add(new Sequence("Die Rolle des Messprozesses in der Quantenphysik", null));

        expectedTopicList.add(new Topic("Grundlagen", "Welche Vorstellungen darf man von einer Welt haben, die man nicht sehen kann, nicht hören kann, nicht erleben kann?Was sind Objekte sind Quantenobjekte? Was lässt sie eine solche Faszination ausstrahelen? Fragen über Fragen von denen zunächst die grundlegendsten geklärt werden sollen.", expectedSequenceList));
    }

    @Test
    public void getTopics() throws Exception {
        db.open();
        List<Topic> topicList = db.getTopics();
        List<Sequence> sequenceList = topicList.get(0).getSequencelist();
        List<Unit> unitList = sequenceList.get(0).getUnitList();

        assertEquals("Title of Topics should be the same",
                expectedTopicList.get(0).getTitle(),
                topicList.get(0).getTitle());

        assertEquals( "title of seq1 should be the same",
                expectedTopicList.get(0).getSequencelist().get(0).getTitle(),
                sequenceList.get(0).getTitle()
                );

        assertEquals("title of unit1 of seq1 should be the same",
                expectedTopicList.get(0).getSequencelist().get(0).getUnitList().get(0).getTitle(),
                unitList.get(0).getTitle()
                );

        assertEquals("question should be the same",
                expectedUnitList.get(0).getQuestion().getQuesiton(),
                unitList.get(0).getQuestion().getQuesiton()
                );

        assertEquals("content should be the same",
                expectedUnitList.get(0).getContent().getContent(),
                unitList.get(0).getContent().getContent()
                );

        assertEquals("content should have the correct media",
                expectedUnitList.get(0).getContent().getPathToMedium(),
                unitList.get(0).getContent().getPathToMedium()
                );

        assertEquals("question should have the correct solution",
                expectedUnitList.get(0).getQuestion().getSolution(),
                unitList.get(0).getQuestion().getSolution()
                );

        assertEquals("question should have the correct solution",
                expectedUnitList.get(0).getQuestion().getExplanationForSolution(),
                unitList.get(0).getQuestion().getExplanationForSolution()
        );

        assertEquals("question should have the correct answer options",
                expectedUnitList.get(0).getQuestion().getAnswerOptions(),
                unitList.get(0).getQuestion().getAnswerOptions()
        );


        assertEquals("question should have the correct answer options",
                expectedUnitList.get(1).getQuestion().getAnswerOptions(),
                unitList.get(1).getQuestion().getAnswerOptions()
        );

        assertEquals("questions should have same solutions",
                expectedUnitList.get(0).getQuestion().getSolution(),
                unitList.get(0).getQuestion().getSolution()
                );

        assertEquals("questions should have same solutions",
                expectedUnitList.get(1).getQuestion().getSolution(),
                unitList.get(1).getQuestion().getSolution()
        );

        db.close();
    }

}