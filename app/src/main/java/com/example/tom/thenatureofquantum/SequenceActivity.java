package com.example.tom.thenatureofquantum;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SequenceActivity extends AppCompatActivity {

    private static List<Unit> unitList;

//    private static Feedback feedbackState;
    private static int fragmentCreationPosition;
    private static FloatingActionButton fab;




    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ProgressControllingViewPager mViewPager;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sequence);

        setTitle(TopicManager.getSelectedSequence().getTitle());

        //currentSeqPosition = 0;

        // load list of units
        unitList = TopicManager.getSelectedSequence().getUnitList();

        /*
        Views:
         */
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        fab.setVisibility(View.INVISIBLE);


        // Create the adapter that will return a fragment for each of the primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ProgressControllingViewPager) findViewById(R.id.container);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // TODO minimize the offset when scrolling
                if (position > TopicManager.getSelectedSequence().getProgress() * 2  + 1) {
                    mViewPager.setCurrentItem(TopicManager.getSelectedSequence().getProgress() * 2 + 1);
                }
            }

            @Override
            public void onPageSelected(int position) {
                if (position > TopicManager.getSelectedSequence().getProgress() * 2  + 1) {
                    Toast.makeText(getApplicationContext(), "Die nächste Einheit muss erst freigeschalten werden.", Toast.LENGTH_SHORT).show();
                } else {
                    TopicManager.setCurrentSeqPosition(position / 2);
                    fab.setVisibility(View.INVISIBLE);
//                    Toast.makeText(getApplication(), "position: " + Integer.toString(position / 2), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setAdapter(mSectionsPagerAdapter);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);



//        fab.setVisibility(View.INVISIBLE);
    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sequence, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    /**
     * Content Fragment
     */
    public static class ContentFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_content_number";

        public ContentFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ContentFragment newInstance(int sectionNumber) {
            ContentFragment fragment = new ContentFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sequence_content, container, false);

            TextView tvTitle = (TextView) rootView.findViewById(R.id.unit_label);
            TextView tvContent = (TextView) rootView.findViewById(R.id.textViewContent);

//            fab.setVisibility(View.INVISIBLE); // todo put in resume

            // Imange of Content
            ImageView ivContent = (ImageView) rootView.findViewById(R.id.imageViewContent);
            Resources res = getResources();
            String imageName = unitList.get(fragmentCreationPosition).getContent().getPathToMedium();
            if (imageName == null) {
                ivContent.setVisibility(View.GONE);
            } else {
                int resID = res.getIdentifier(imageName, "drawable", getContext().getPackageName());
                ivContent.setImageResource(resID);
                ivContent.setVisibility(View.VISIBLE);
            }
            // fill fragment with content
            tvTitle.setText(unitList.get(fragmentCreationPosition).getTitle());
            tvContent.setText(unitList.get(fragmentCreationPosition).getContent().getContent());
            return rootView;
        }

//        @Override
//        public void onPause() {
//            if (currentSeqPosition > 0) currentSeqPosition -= 1;
//            super.onPause();
//        }
//
//        @Override
//        public void onResume() {
//            currentSeqPosition += 1;
//            super.onResume();
//        }
    }

    /**
     * Question Fragement
     */
    public static class QuestionFragment extends Fragment {
        /*
        Anouncing Views
         */
        TextView tvTitle;
        TextView tvQuestion;
        TextView tvStatFeedback;

        ImageView ivQuestion;

        LinearLayout llayStatFeedback;

        private CheckBox checkBox1;
        private CheckBox checkBox2;
        private CheckBox checkBox3;
        private CheckBox checkBox4;

        private Button btnQuestion;


        private static final String ARG_SECTION_NUMBER = "section_question_number";
        private Feedback feedbackState;

        public QuestionFragment() {
        }

        public  static QuestionFragment newInstance(int sectionNumber) {
            QuestionFragment fragment = new QuestionFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_sequence_question, container, false);

            // retrieving question Information
            Question question = unitList.get(fragmentCreationPosition).getQuestion();
            Set<AnswerOption> answerOption = question.getAnswerOptions();
            Iterator answerOptIterator = answerOption.iterator();

            // initilizing views
            tvTitle = (TextView) rootView.findViewById(R.id.unit_label);
            tvQuestion = (TextView) rootView.findViewById(R.id.textViewQuestion);
            ivQuestion = (ImageView) rootView.findViewById(R.id.imageViewQuestion);

            // static Feedback
            tvStatFeedback = (TextView) rootView.findViewById(R.id.textViewStaticFeedback);
            llayStatFeedback = (LinearLayout) rootView.findViewById(R.id.linearLayoutStaticFeedback);
            llayStatFeedback.setVisibility(View.GONE);

            // Meida for Question
            Resources res = getResources();
            String imageName = unitList.get(fragmentCreationPosition).getQuestion().getPathToMedium();
            if (imageName == null) {
                ivQuestion.setVisibility(View.GONE);
            } else {
                int resID = res.getIdentifier(imageName, "drawable", getContext().getPackageName());
                ivQuestion.setImageResource(resID);
                ivQuestion.setVisibility(View.VISIBLE);
            }

            // checkboxes for answer
            checkBox1 = (CheckBox) rootView.findViewById(R.id.checkBox);
            checkBox2 = (CheckBox) rootView.findViewById(R.id.checkBox2);
            checkBox3 = (CheckBox) rootView.findViewById(R.id.checkBox3);
            checkBox4 = (CheckBox) rootView.findViewById(R.id.checkBox4);
            btnQuestion = (Button) rootView.findViewById(R.id.buttonQuestion);

            // fill views of fragment with content
            tvTitle.setText(unitList.get(fragmentCreationPosition).getTitle());
            tvQuestion.setText(question.getQuesiton());
            // if iterator has no succesor make checkbox invisible:
            if (answerOptIterator.hasNext()) checkBox1.setText(answerOptIterator.next().toString());
            else checkBox1.setVisibility(View.INVISIBLE);
            if (answerOptIterator.hasNext()) checkBox2.setText(answerOptIterator.next().toString());
            else checkBox2.setVisibility(View.INVISIBLE);
            if (answerOptIterator.hasNext()) checkBox3.setText(answerOptIterator.next().toString());
            else checkBox3.setVisibility(View.INVISIBLE);
            if (answerOptIterator.hasNext())checkBox4.setText(answerOptIterator.next().toString());
            else checkBox4.setVisibility(View.INVISIBLE);

            /*
            FB
             */
//            todo Realize state pattern
            feedbackState = new DynamicFeedback(question); // todo move from oncreate away

            btnQuestion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    feedbackState.giveFeedback();
                }
            });


//            btnQuestion.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Set<String> givenSolution = new HashSet<String>();
//
//                    if (checkBox1.isChecked()) givenSolution.add(checkBox1.getText().toString());
//                    if (checkBox2.isChecked()) givenSolution.add(checkBox2.getText().toString());
//                    if (checkBox3.isChecked()) givenSolution.add(checkBox3.getText().toString());
//                    if (checkBox4.isChecked()) givenSolution.add(checkBox4.getText().toString());
//
//                    if (unitList.get(TopicManager.getCurrentSeqPosition()).getQuestion().getSolution().equals(givenSolution)) {
//                        // todo next slide
//                        if (TopicManager.getSelectedSequence().getProgress() == TopicManager.getCurrentSeqPosition()) {
//                            TopicManager.getSelectedSequence().riseProgress();
//                            Toast.makeText(getContext(), "Good!", Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(getContext(), "Well remembered.", Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        // todo give FB
//                        Toast.makeText(getContext(), "shit, currentSeqPos: " + TopicManager.getCurrentSeqPosition()  + " "
//                                + givenSolution + " " + unitList.get(TopicManager.getCurrentSeqPosition()).getQuestion().getSolution(), Toast.LENGTH_SHORT).show();
//                    }
//                }
//            });

            return rootView;
        }


        /**
         * Feedbacksystem ITF-Model
         * realized by State-Pattern
         */
        public abstract class Feedback {

            protected Question question;

            public Feedback(Question question) {
                this.question = question;
            }

            public void giveFeedback() {
                throw new IllegalStateException();
            }
        }


        public class DynamicFeedback extends Feedback {

            public DynamicFeedback(Question question) {
                super(question);
            }

            @Override
            public void giveFeedback() {

                final Map<Integer,String> dynamicFeedbackMassages;
                Set<String> givenSolution = new HashSet<String>();

                DatabaseAccess db = DatabaseAccess.getInstance(getContext());
                db.open();
                dynamicFeedbackMassages = db.getDynamicFeedback();
                db.close();

                if (checkBox1.isChecked()) givenSolution.add(checkBox1.getText().toString());
                if (checkBox2.isChecked()) givenSolution.add(checkBox2.getText().toString());
                if (checkBox3.isChecked()) givenSolution.add(checkBox3.getText().toString());
                if (checkBox4.isChecked()) givenSolution.add(checkBox4.getText().toString());

                Set<String> solution = new HashSet<>();
                for (AnswerOption aOpt : question.getAnswerOptions()) {
                    if (aOpt.isCorrect()) solution.add(aOpt.getanswerOptionText());
                }


                if (solution.equals(givenSolution)) {
                    feedbackState = new SolutionState(question);
                    feedbackState.giveFeedback();

                } else {
                    Set<Integer> dynFeebackKeys = TopicManager.getSelectedSequence().getSelectedUnit().getQuestion().getDynFeedback();
                    // TODO TEMPORARY SOLUTION -> COMPETENCE MAPPING NECESSARY
                    String massage = "";
                    for (Integer i : dynFeebackKeys) {
                        massage = "Nachricht " + i + ": " + dynamicFeedbackMassages.get(i) + "\n";
                    }
                    final String m = massage;  //TODO just not a nice solution, but for the snackbar, it needs to be final

                    if (massage.isEmpty()) {
                       Toast.makeText(getContext(),"Leider falsch. Versuche es nochmal.", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            fab.setVisibility(View.VISIBLE);
                            fab.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Snackbar snackbar = Snackbar.make(getView(), m, Snackbar.LENGTH_LONG); //.setAction("Action", null).show();
                                    View snackbarView = snackbar.getView();
                                    TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
                                    textView.setMaxLines(5);
                                    snackbar.setAction("Action", null).show();

                                }
                            });

                            feedbackState = new StaticFeedback(question);

                        } catch (Exception e) {
                            // do nothing
                            // todo correct exception treatment
                        }
                    }
                }

            }
        }


        public class StaticFeedback extends Feedback {

            Iterator<AnswerOption> statFBIterator;
            String statFBMassege = "";

            public StaticFeedback(Question question) {
                super(question);
            }

            @Override
            public void giveFeedback() {
                Set<String> givenSolution = new HashSet<String>();

                if (checkBox1.isChecked()) givenSolution.add(checkBox1.getText().toString());
                if (checkBox2.isChecked()) givenSolution.add(checkBox2.getText().toString());
                if (checkBox3.isChecked()) givenSolution.add(checkBox3.getText().toString());
                if (checkBox4.isChecked()) givenSolution.add(checkBox4.getText().toString());

                // used for checking whether answer was correct or not
                Set<String> solution = new HashSet<>();
                for (AnswerOption aOpt : question.getAnswerOptions()) {
                    if (aOpt.isCorrect()) solution.add(aOpt.getanswerOptionText());
                }
                // used for getting feedbackmassage
                Set<AnswerOption> answersForStatFB = new HashSet<>();
                for (AnswerOption aOpt : question.getAnswerOptions()) {
                    if (!aOpt.isCorrect() && givenSolution.contains(aOpt.getanswerOptionText())) answersForStatFB.add(aOpt);
                }
                // used for coloring background
                Set<String> wrongAnswers = new HashSet<>();
                for (AnswerOption aOpt : answersForStatFB){
                    wrongAnswers.add(aOpt.getanswerOptionText());
                }

                if (solution.equals(givenSolution)) {
                    feedbackState = new SolutionState(question);
                    feedbackState.giveFeedback();

//                } else if (statFBIterator != null && !statFBIterator.hasNext()) {
//                    Toast.makeText(getContext(), "Vielleicht hilft der Hinweis weiter?", Toast.LENGTH_SHORT).show();
                } else {
                    // flagging errors
                    if (wrongAnswers.contains(checkBox1.getText())) checkBox1.setBackgroundColor(Color.parseColor("#FAD870"));
                    if (wrongAnswers.contains(checkBox2.getText())) checkBox2.setBackgroundColor(Color.parseColor("#FAD870"));
                    if (wrongAnswers.contains(checkBox3.getText())) checkBox3.setBackgroundColor(Color.parseColor("#FAD870"));
                    if (wrongAnswers.contains(checkBox4.getText())) checkBox4.setBackgroundColor(Color.parseColor("#FAD870"));

                    // generating massage
                    // from false answers
                    statFBIterator = answersForStatFB.iterator();
                    while (statFBIterator.hasNext()) {
                        String massage = statFBIterator.next().getStaFB();
                        if (!tvStatFeedback.getText().toString().contains(massage) && !statFBMassege.contains(massage)) {
                            statFBMassege += massage + "\n \n";
                        }
                    }
                    // from any answer, if there was no false selected, just not all
                    if (statFBMassege.isEmpty()) {
                       Toast.makeText(getContext(), "Es sind mehrere Antworten richtig.", Toast.LENGTH_SHORT).show();
                    }

                    // just show if some massage was generated
                    if (!statFBMassege.isEmpty()) {
                        tvStatFeedback.setText(statFBMassege);
                        llayStatFeedback.setVisibility(View.VISIBLE);
                    }

                    // TODO focus is not working yet
                    tvStatFeedback.setFocusable(true);
                    tvStatFeedback.setFocusableInTouchMode(true);
                    tvStatFeedback.requestFocus();
//                    tvStatFeedback.setSelected(true);
                    //                    // todo static Feedback
//                    Toast.makeText(getContext(), "shit, currentSeqPos: " + TopicManager.getCurrentSeqPosition()  + " "
//                            + givenSolution + " " + question.getSolution(), Toast.LENGTH_SHORT).show();
                }

            }
        }


        public class SolutionState extends Feedback {

            public SolutionState(Question question) {
                super(question);
            }

            @Override
            public void giveFeedback() {

                Set<String> givenSolution = new HashSet<String>();

                if (checkBox1.isChecked()) givenSolution.add(checkBox1.getText().toString());
                if (checkBox2.isChecked()) givenSolution.add(checkBox2.getText().toString());
                if (checkBox3.isChecked()) givenSolution.add(checkBox3.getText().toString());
                if (checkBox4.isChecked()) givenSolution.add(checkBox4.getText().toString());

                Set<String> solution = new HashSet<>();
                for (AnswerOption aOpt : question.getAnswerOptions()) {
                    if (aOpt.isCorrect()) solution.add(aOpt.getanswerOptionText());
                }


                if (solution.equals(givenSolution)) {

                    // todo move to next slide automaticcally
                    if (TopicManager.getSelectedSequence().getProgress() == TopicManager.getCurrentSeqPosition()) {
                        TopicManager.getSelectedSequence().riseProgress();
                        Toast.makeText(getContext(), "Good!", Toast.LENGTH_SHORT).show();

                        if (solution.contains(checkBox1.getText())) checkBox1.setBackgroundColor(Color.parseColor("#70FA93"));
                        if (solution.contains(checkBox2.getText())) checkBox2.setBackgroundColor(Color.parseColor("#70FA93"));
                        if (solution.contains(checkBox3.getText())) checkBox3.setBackgroundColor(Color.parseColor("#70FA93"));
                        if (solution.contains(checkBox4.getText())) checkBox4.setBackgroundColor(Color.parseColor("#70FA93"));

                        // update Topic
                        if (TopicManager.getCurrentSeqPosition() + 1 == TopicManager.getSelectedSequence().getUnitList().size())
                            TopicManager.getSelectedTopic().riseProgress();
                    } else {
                        Toast.makeText(getContext(), "Well remembered.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    feedbackState = new StaticFeedback(question);
                    feedbackState.giveFeedback();
                }
            }
        }

        @Override
        public void onResume() {
            super.onResume();
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a ContentFragment (defined as a static inner class below).

            fragmentCreationPosition = position / 2;
//            Toast.makeText(getApplication(), "position: " + Integer.toString(position), Toast.LENGTH_SHORT).show();
            switch (position % 2){
                case 0:
                    return ContentFragment.newInstance(position);
                default:

                    // UNLOCKING A NEW UNIT
//                    if (position > currentSequence.getProgress())
//                        currentSequence.riseProgress();
                    //updateing current unit
                    //currentSequence.setCurrentUnit(position - 1);
                    return QuestionFragment.newInstance(position);
            }

        }

        @Override
        public int getCount() {
            return unitList.size() * 2;
        }


        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return ">";
        }
    }



}
