package com.example.tom.thenatureofquantum;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

/**
 * Created by tom on 02.08.17.
 */

public class TopicArrayAdapter extends ArrayAdapter<Topic> {

    private Context context;
    private List<Topic> topicList;

    public TopicArrayAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Topic> objects) {
        super(context, resource, objects);
        this.context = context;
        this.topicList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Topic topic = topicList.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.topiclayout, null);

        TextView tvTitle = (TextView) view.findViewById(R.id.textViewTopicTitle);
        TextView tvAbstract = (TextView) view.findViewById(R.id.textViewTopicAbstract);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarTopic);
        LinearLayout llayTopic = (LinearLayout) view.findViewById(R.id.llayTopic);

        tvTitle.setText(topic.getTitle());
        tvAbstract.setText(topic.getAbstract_text());
        progressBar.setMax(topic.getSequencelist().size());
        progressBar.setProgress(topic.getProgress());

//        tvTitle.setBackgroundColor(Color.parseColor("#ff99cc00"));
//        tvAbstract.setBackgroundColor(Color.parseColor("#ff99cc00"));
//        progressBar.setBackgroundColor(Color.parseColor("#ff99cc00"));
        llayTopic.setBackgroundColor(Color.parseColor("#ff99cc00"));

        return view;
    }
}
