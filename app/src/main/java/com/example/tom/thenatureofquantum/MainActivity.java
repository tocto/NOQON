package com.example.tom.thenatureofquantum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

//import static com.example.tom.thenatureofquantum.R.id.button_profil_temp;
import static com.example.tom.thenatureofquantum.R.id.default_activity_button;
import static com.example.tom.thenatureofquantum.R.id.listViewTopics;

public class MainActivity extends AppCompatActivity {

    private Button btnProfilView;
    private ListView lvTopics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize Learning Content
        TopicManager.initializeTopics(this);

        initializeActivity();
    }

    @Override
    protected void onResume() {
        initializeActivity();
        super.onResume();
    }

    public void initializeActivity(){
        // initializing GUI Elements
//        btnProfilView = (Button) findViewById(button_profil_temp);
        lvTopics = (ListView) findViewById(listViewTopics);

        // filling ListView with Topics
        ArrayAdapter<Topic> topicAdapter = new TopicArrayAdapter(this, 0, TopicManager.getTopicList());
        lvTopics.setAdapter(topicAdapter);

//        /*
//        Button Controlling
//         */
//        // Open ProfilView
//        btnProfilView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, ProfilActivity.class);
//                startActivity(intent);
//            }
//        });

        // Selecting Topics
        lvTopics.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), TopicActivity.class);
                //intent.putExtra("topicID", i);
                TopicManager.setSelectedTopic(TopicManager.getTopicList().get(i));
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.profile:
                Intent intent = new Intent(MainActivity.this, ProfilActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}