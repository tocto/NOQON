package com.example.tom.thenatureofquantum;

import java.util.List;
import java.util.Set;

/**
 * Created by tom on 26.07.17.
 */

public class Question {

    private String quesiton;
    private Set<AnswerOption> answerOptions;
    private String pathToMedium;
    private Set<Integer> dynFeedback;

    public Question(String question, Set<AnswerOption> answerOptions, String pathToMedium, Set<Integer> dynFeedback) {
        this.quesiton = question;
        this.answerOptions = answerOptions;
        this.pathToMedium = pathToMedium;
        this.dynFeedback = dynFeedback;
    }

    public String getQuesiton() {
        return quesiton;
    }

    public Set<AnswerOption> getAnswerOptions() {
        return answerOptions;
    }

    public String getPathToMedium() {
        return pathToMedium;
    }

    public Set<Integer> getDynFeedback() {
        return dynFeedback;
    }


}
