package com.example.tom.thenatureofquantum;

/**
 * Created by tom on 26.07.17.
 */

public class Content {

    private String content;
    private String pathToMedium;

    public Content(String content, String pathToMedium) {
        this.content = content;
        this.pathToMedium = pathToMedium;
    }

    public String getContent() {
        return content;
    }

    public String getPathToMedium() {
        return pathToMedium;
    }

}
