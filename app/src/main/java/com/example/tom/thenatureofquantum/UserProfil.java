package com.example.tom.thenatureofquantum;

/**
 * Created by tom on 14.08.17.
 */

public class UserProfil {
    private String username;
    private int score;
    private String pathProfilPicture;
    private int level;


    public UserProfil(String username) {
        this.username = username;
        this.level = 1;
        this.score = 0;
    }

    public void setPathProfilPicture(String pathProfilPicture) {
        this.pathProfilPicture = pathProfilPicture;
    }

    public String getPathProfilPicture() {
        return pathProfilPicture;
    }

    public String getUsername() {
        return username;
    }

    public int getScore() {
        return score;
    }

    public int getLevel() {
        return level;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void addXPtoScore(int xp){
        this.score += xp;
        if (score%100 == 0) level += 1; //todo change rising level
    }
}
