package com.example.tom.thenatureofquantum;

import java.util.List;

/**
 * Created by tom on 26.07.17.
 */

public class Sequence {

    private String title;
    private String abstractText;
    private int progress = 0; // from 0 to (unit.size() - 1)
    private List<Unit> unitList;
    private Unit selectedUnit;

    public Sequence(String title,String abstractText, List<Unit> unitlist){
        this.title = title;
        this.abstractText = abstractText;
        this.unitList = unitlist;
        this.selectedUnit = unitlist.get(0);
    }

    public String getTitle() {
        return title;
    }

    public int getProgress() {
        return progress;
    }

    public void riseProgress() {
        this.progress += 1;
    }

    public List<Unit> getUnitList() {
        return unitList;
    }

    public Unit getSelectedUnit() {
        return selectedUnit;
    }

    public String getAbstractText() {
        return abstractText;
    }

    //    public int getProgressPositionOfSelectedUnit() {
//        int i = 1;
//        for (Unit u : unitList) {
//            if (u.getTitle().equals(selectedUnit.getTitle())) break;
//            i++;
//        }
//        return i;
//    }

    public void setSelectedUnit(int number) {
        this.selectedUnit = unitList.get(number);
    }


}
