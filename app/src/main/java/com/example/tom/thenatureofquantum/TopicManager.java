package com.example.tom.thenatureofquantum;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tom on 27.07.17.
 */

public class TopicManager {

    private static List<Topic> topicList;
    private static Topic selectedTopic;
    private static int currentSeqPosition;
    private UserProfil userProfil;
    private TopicManager topicManager;

    private TopicManager() {
        // singelton
    }

    public TopicManager getInstance(){
        return this.topicManager;
    }

    public void setUserProfil(UserProfil userProfil) {
        this.userProfil = userProfil;
    }

    public UserProfil getUserProfil() {
        return userProfil;
    }

    /**
     * Loading all Topics from Database
     *
     * @param context
     */
    public static void initializeTopics(Context context) {
        // read list of Topics from Database
        DatabaseAccess db = DatabaseAccess.getInstance(context);
        db.open();
        topicList = db.getTopics();
    }

    public static List<Topic> getTopicList(){
        return topicList;
    }

    public static Topic getSelectedTopic() {
        return selectedTopic;
    }

    public static void setSelectedTopic(Topic selectedTopic) {
        TopicManager.selectedTopic = selectedTopic;
        currentSeqPosition = 0; // resets current suquence Position
    }

    public static Sequence getSelectedSequence() {
        return  selectedTopic.getSelectedSequence();
    }

    public static void setSelectedSequence(int seqPos) {
        selectedTopic.setSelectedSequence(selectedTopic.getSequencelist().get(seqPos));
    }


    public static int getCurrentSeqPosition() {
        return currentSeqPosition;
    }

    public static void setCurrentSeqPosition(int currentSeqPosition) {
        TopicManager.currentSeqPosition = currentSeqPosition;
        TopicManager.getSelectedSequence().setSelectedUnit(currentSeqPosition);
    }

    //    public static Unit getSelectedUnit() {
//        return selectedTopic.getSelectedSequence().getSelectedUnit();
//    }
}
