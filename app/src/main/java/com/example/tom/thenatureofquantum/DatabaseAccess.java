package com.example.tom.thenatureofquantum;

/**
 * Created by tom on 22.07.17.
 */

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ArrayAdapter;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all quotes from the database.
     *
     * @return a List of quotes
     */
    public List<Topic> getTopics() {
        List<Topic> topiclist = new ArrayList<>();
        List<Sequence> sequencelist = new ArrayList<>();
        List<Unit> unitList = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM topics", null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            // Todo: for each topic generate the belonging list of sequences
            Topic topic = new Topic(cursor.getString(1), cursor.getString(2), getSequences(cursor.getInt(0)));
            topiclist.add(topic);
            cursor.moveToNext();
        }
        cursor.close();
        return topiclist;
    }

    private List<Sequence> getSequences(int topicID) {
        List<Sequence> listOfSeq = new ArrayList<>();
        // todo add selection for db query
        Cursor cursor = database.rawQuery("SELECT * FROM sequences WHERE topicID=?", new String[] {Integer.toString(topicID)}); // WHERE topicID=?",new String[] {topicCursor.getString(0)});
        cursor.moveToFirst();//String[] columns = {"title"};

        while (!cursor.isAfterLast()) {
            Sequence seq = new Sequence(cursor.getString(2), cursor.getString(3), getUnits(cursor.getInt(0)));
            listOfSeq.add(seq);
            cursor.moveToNext();
        }
        return listOfSeq;
    }

    private  List<Unit> getUnits(int seqID) {
        List<Unit> listOfUnits = new ArrayList<>();
        // declaring Cursors to create Unit with Content and Question
        Cursor cursorUnit;
        Cursor cursorContent;
        Cursor cursorQuestion;
        Cursor cursorAnswerOptions;
        // declaring attributes for unit
        String unitTitle;
        int xp;
        Content content;
        Question question;
        Set<AnswerOption> answerOptList;
        Set<String> correctAnswerList;

        try {
            // selecting details from unit table
            cursorUnit = database.rawQuery("SELECT * FROM units WHERE sequenceID=?", new String[] {Integer.toString(seqID)});
            cursorUnit.moveToFirst();
            if (cursorUnit.getCount() == 0) throw new NullPointerException("There are no units in the sequence with id=" + seqID);

            while (!cursorUnit.isAfterLast()) {
                // setting cursors for content and question
                cursorContent = database.rawQuery("SELECT * FROM contents WHERE unitID=?", new String[] {Integer.toString(cursorUnit.getInt(0))});
                cursorContent.moveToFirst();
                cursorQuestion = database.rawQuery("SELECT * FROM questions WHERE unitID=?", new String[] {Integer.toString(cursorUnit.getInt(0))});
                cursorQuestion.moveToFirst();
                cursorAnswerOptions = database.rawQuery("SELECT * FROM answer_options WHERE unitID=?", new String[] {Integer.toString(cursorUnit.getInt(0))});
                cursorAnswerOptions.moveToFirst();

                // getting answer Options for Question
                answerOptList = new HashSet<>();
                correctAnswerList = new HashSet<>();
                boolean correctness = false;
                while (!cursorAnswerOptions.isAfterLast()) {
                    int correctnessInteger = cursorAnswerOptions.getInt(3);
                    if (correctnessInteger != 0) correctness = true;
                    else correctness = false;
                    AnswerOption answerOption = new AnswerOption(cursorAnswerOptions.getString(2), cursorAnswerOptions.getString(4),  correctness);
                    answerOptList.add(answerOption);
                    cursorAnswerOptions.moveToNext();
                }

                // get dynamic Feedback competencies of question
                Set competenceSet = new HashSet();
                String[] competencies = cursorQuestion.getString(5).split(",");
                for (String competence : competencies){
                    competenceSet.add(Integer.parseInt(competence));
                }

                // create unit
                unitTitle = cursorUnit.getString(3);
                xp = cursorUnit.getInt(2);
                // content
                content = new Content(cursorContent.getString(1), cursorContent.getString(2));
                // question
                question = new Question(cursorQuestion.getString(1), answerOptList, cursorQuestion.getString(4),competenceSet);
                // media


                // create Unit
                Unit unit = new Unit(unitTitle, content, question);

                listOfUnits.add(unit);
                cursorUnit.moveToNext();
            }

        } finally {
            return listOfUnits;
        }
    }

    public Map<Integer,String> getDynamicFeedback() {

        Map<Integer, String> dynFB = new HashMap<>();

        Cursor cursor = database.rawQuery("SELECT * FROM dynamicFeedback", null); // WHERE topicID=?",new String[] {topicCursor.getString(0)});
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            dynFB.put(cursor.getInt(1), cursor.getString(2));
            cursor.moveToNext();
        }

        return dynFB;
    }

}
