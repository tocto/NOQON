package com.example.tom.thenatureofquantum;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Toast;

/**
 * Created by tom on 30.07.17.
 */

public class ProgressControllingViewPager extends ViewPager {

    private boolean scrollingEnabled = true;
    private int position;

    public ProgressControllingViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        // todo swipe to the left should be possible

//        if (TopicManager.getCurrentSeqPosition() <= TopicManager.getSelectedSequence().getProgress()){
//            return super.onInterceptTouchEvent(ev);
//        }
//        else {
//            return false;
//        }

        return super.onInterceptTouchEvent(ev);
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
