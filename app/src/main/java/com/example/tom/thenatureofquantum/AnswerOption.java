package com.example.tom.thenatureofquantum;

/**
 * Created by tom on 27.08.17.
 */

public class AnswerOption {
    private String answerOptionText;
    private String staFB;
    private boolean correct;

    public AnswerOption(String answerOptionText, String staFB, boolean correctness) {
        this.answerOptionText = answerOptionText;
        this.staFB = staFB;
        this.correct = correctness;
    }

    public String getanswerOptionText() {
        return answerOptionText;
    }

    public String getStaFB() {
        return staFB;
    }

    public boolean isCorrect() {
        return correct;
    }

    @Override
    public String toString() {
        return answerOptionText;
    }
}
