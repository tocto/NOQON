package com.example.tom.thenatureofquantum;

/**
 * Created by tom on 26.07.17.
 */

public class Unit {

    private int xp = 10;  // 10 is the default value for a unit
    private String title;
    private Content content;
    private Question question;


    public Unit(String title, Content content, Question question) {
        this.title = title;
        this.content = content;
        this.question = question;
    }

    public String getTitle() {
        return title;
    }

    public Content getContent() {
        return content;
    }

    public Question getQuestion() {
        return question;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }
}
