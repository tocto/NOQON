package com.example.tom.thenatureofquantum;

import java.util.List;

/**
 * Created by tom on 26.07.17.
 */

public class Topic {

    private String title;
    private String abstract_text;
    private int progress;
    private List<Sequence> sequencelist;
    private Sequence selectedSequence;

    public Topic(String title, String abstract_text, List<Sequence> sequencelist) {
        this.title = title;
        this.abstract_text = abstract_text;
        this.sequencelist = sequencelist;
    }

    public String getTitle() {
        return title;
    }

    public String getAbstract_text() {
        return abstract_text;
    }

    public int getProgress() {
        return progress;
    }

    public void riseProgress() {
        this.progress += 1;
    }

    public List<Sequence> getSequencelist() {
        return sequencelist;
    }

    public Sequence getSelectedSequence() {
        return selectedSequence;
    }

    public void setSelectedSequence(Sequence selectedSequence) {
        this.selectedSequence = selectedSequence;
    }
}
