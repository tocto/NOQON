package com.example.tom.thenatureofquantum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static com.example.tom.thenatureofquantum.R.id.listViewSequences;

public class TopicActivity extends AppCompatActivity {

    //Views
    private ListView lvSeq;

    int topicID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);

        initializeActivity();
    }

    @Override
    protected void onResume() {
        initializeActivity();
        super.onResume();
    }

    private void initializeActivity(){
        setTitle(TopicManager.getSelectedTopic().getTitle());

        lvSeq = (ListView) findViewById(listViewSequences);

        // fill ListView
        ArrayAdapter<Sequence> topicAdapter = new SequenceArrayAdapter(this, 0, TopicManager.getSelectedTopic().getSequencelist());
        lvSeq.setAdapter(topicAdapter);

        /*
        Button handlers
         */
        lvSeq.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (TopicManager.getSelectedTopic().getProgress() >= i) {
                    Intent intent = new Intent(getApplicationContext(), SequenceActivity.class);
//                intent.putExtra("topicID", topicID);
//                intent.putExtra("sequenceID", i);

//                Sequence selectedSequence = TopicManager.getSelectedTopic().getSequencelist().get(i);

                    // update TopicManager with selected Sequence
                    TopicManager.setSelectedSequence(i);
//                TopicManager.getSelectedTopic().setSelectedSequence(
//                        TopicManager.getSelectedTopic().getSequencelist().get(i));
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Diese Sequenz ist noch nicht freigeschaltet.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
