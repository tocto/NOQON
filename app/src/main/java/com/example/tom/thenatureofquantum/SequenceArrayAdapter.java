package com.example.tom.thenatureofquantum;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

/**
 * Created by tom on 02.08.17.
 */

public class SequenceArrayAdapter extends ArrayAdapter<Sequence> {

    private Context context;
    private List<Sequence> seqList;

    public SequenceArrayAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.context = context;
        this.seqList = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Sequence seq = seqList.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.sequencelistlayout, null);

        TextView tvTitle = (TextView) view.findViewById(R.id.textViewSeqTitle);
        TextView tvAbstract = (TextView) view.findViewById(R.id.textViewSeqAbstract);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBarSeq);

        tvTitle.setText(seq.getTitle());
        tvAbstract.setText(seq.getAbstractText());
        progressBar.setMax(seq.getUnitList().size());
        progressBar.setProgress(seq.getProgress());

        // looking unlocked Sequencies
        if (TopicManager.getSelectedTopic().getProgress() >= position) {
            tvTitle.setBackgroundColor(Color.parseColor("#ff99cc00"));
            tvAbstract.setVisibility(View.VISIBLE);
            tvAbstract.setBackgroundColor(Color.parseColor("#ff99cc00"));
        }

        return view;
    }
}
